#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    '''
    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n2.4\n2.4\n2.4\n0.90\n")
    '''
  
    def test_movieID_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r,w)
        result = w.getvalue()
        movieID = ''
        for c in result:
            if (c != ':'):
                movieID += c
            else:
                break
        self.assertEqual(movieID, "10040")
    
    def test_movieID_2(self):
        r = StringIO("10:\n1952305\n1531863\n")
        w = StringIO()
        netflix_eval(r,w)
        result = w.getvalue()
        movieID = ''
        for c in result:
            if (c != ':'):
                movieID += c
            else:
                break
        self.assertEqual(movieID, "10")

    def test_rmse_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r,w)
        result = w.getvalue()
        rmse = float (result[-5:-1])
        self.assertTrue(rmse < 1.0)
    
    def test_rmse_2(self):
        r = StringIO("10:\n1952305\n1531863\n")
        w = StringIO()
        netflix_eval(r,w)
        result = w.getvalue()
        rmse = float (result[-5:-1])
        self.assertTrue(rmse < 1.0)
    
    def test_rmse_3(self):
        r = StringIO("1000:\n2326571\n977808\n1010534\n1861759\n79755\n98259\n1960212\n97460\n2623506\n2409123\n1959111\n809597\n2251189\n537705\n929584\n506737\n708895\n1900790\n2553920\n1196779\n2411446\n1002296\n1580442\n100291\n433455\n2368043\n906984\n1:\n30878\n2647871\n1283744\n2488120\n317050\n1904905\n1989766\n14756\n1027056\n1149588\n1394012\n1406595\n2529547\n1682104\n2625019\n2603381\n1774623\n470861\n712610\n1772839\n1059319\n2380848\n548064")
        w = StringIO()
        netflix_eval(r,w)
        result = w.getvalue()
        rmse = float (result[-5:-1])
        self.assertTrue(rmse < 1.0)
# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
